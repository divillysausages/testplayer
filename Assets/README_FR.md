# Devoirs

Créez un nouveau fichier, *Player.cs* dans le dossier *Assets/Scripts* afin qu'il réussisse tous les tests unitaires dans *Test/TestPlayer.cs*.

Pour exécuter les tests, ouvrez *Fenêtre > Général > Test Runner*, puis choisissez *Tout exécuter*.

Vous serez marqué en fonction:

- réussir tous les tests
- implémentation de Player.cs
- lisibilité du code

# Player.cs

*Player.cs* est une classe qui contient:

- Une méthode statique, `Create( string )` qui prend un paramètre `string` (nom). Cette méthode créera un nouveau `Player`, définira sa propriété `name` avec la `string` fournie, et définira une propriété *incrémentielle* `id`, commençant à `0` (c'est-à-dire que le premier `Player` créé a un `id` de `0`, le second a un `id` de `1`, etc.)
- `name` est une propriété en lecture seule - elle ne peut pas être modifiée
- `id` est une propriété en lecture seule - elle ne peut pas être modifiée
- La création d'un nouveau `Player` via le constructeur entraînera `null` pour le nom, et `-1` pour l'`id`.
- Le `Player` a une liste d'amis, qui sont aussi des` Players`
- `Player` a un getter `numFriends` pour renvoyer le nombre d'amis qu'ils ont
- `Player` a une méthode `AddFriend( Player )` qui prendra un `Player` comme paramètre, et les ajoutera à la liste d'amis s'ils ne sont pas déjà là
- `Player` a une méthode `GetFriend( int )` qui prend un paramètre `int` (`id`) et renvoie l'ami avec cet `id`, si nous les avons. Sinon, il retournera `null`
- Le `Player` a un *inventaire* où nous pouvons stocker des éléments en utilisant une clé `string`
- `Player` a une méthode `AddItem( string )` qui prend un paramètre `string` (l'item à ajouter) et l'ajoute à notre inventaire. `AddItem(string)` peut être appelé plusieurs fois pour ajouter plus du même item
- `Player` a une méthode `HasItem( string )` qui prend un paramètre `string` et renvoie `true` ou `false` selon que nous avons l'élément ou non
- `Player` a une méthode `GetNumItems( string )` qui prend un paramètre `string` et renvoie le numéro de cet élément que nous détenons
- `Player` a une méthode `RemoveItem( string )` qui prend un paramètre `string` et supprime un élément si nous l'avons. `RemoveItem( string )` peut être appelé plusieurs fois pour supprimer plus du même élément