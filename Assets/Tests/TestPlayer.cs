﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestPlayer
    {
        // tests creating a simple player using the static factory method
        [Test]
        public void TestCreatePlayer()
        {
            Player player1 = Player.Create( "Joe" );
            Assert.NotNull( player1 );
            Assert.AreEqual( player1.name, "Joe" );

            // test that we can't just create a player
            Player player2 = new Player();
            Assert.IsNull( player2.name );
            Assert.AreEqual( player2.id, -1 );
        }

        // tests creating a second player and that it's different
        // from the first
        [Test]
        public void TestCreateOtherPlayers()
        {
            Player player1 = Player.Create( "Joe" );
            Player player2 = Player.Create( "Bob" );
            Assert.AreNotSame( player1, player2 );
            Assert.Greater( player2.id, player1.id );
            Assert.AreEqual( player2.id, player1.id + 1 );
        }

        // tests adding a player as a friend
        [Test]
        public void TestAddFriends()
        {
            Player player1 = Player.Create( "Joe" );
            Assert.AreEqual( player1.numFriends, 0 );

            Player player2 = Player.Create( "Bob" );
            player1.AddFriend( player2 );
            Assert.AreEqual( player1.numFriends, 1 );

            // test re-adding them
            player1.AddFriend( player2 );
            Assert.AreEqual( player1.numFriends, 1 );
        }

        // tests getting a friend
        [Test]
        public void TestGetFriends()
        {
            Player player1 = Player.Create( "Joe" );
            Player player2 = Player.Create( "Bob" );

            player1.AddFriend( player2 );

            Player friend = player1.GetFriend( player2.id );
            Assert.NotNull( friend );
            Assert.AreSame( friend, player2 );

            friend = player1.GetFriend( -1 );
            Assert.IsNull( friend );
        }

        // tests adding items
        [Test]
        public void TestAddItem()
        {
            Player player1 = Player.Create( "Joe" );

            string item = "potion";

            Assert.IsFalse( player1.HasItem( item ) );

            player1.AddItem( item );
            Assert.IsTrue( player1.HasItem( item ) );
            Assert.AreEqual( player1.GetNumItems( item ), 1 );

            player1.AddItem( item );
            Assert.AreEqual( player1.GetNumItems( item ), 2 );

            // check for an item that we DON'T have
            item = "sword";
            Assert.IsFalse( player1.HasItem( item ) );
            Assert.AreEqual( player1.GetNumItems( item ), 0 );
        }

        // tests removing an item
        [Test]
        public void TestRemoveItem()
        {
            Player player1 = Player.Create( "Joe" );

            string item = "potion";

            Assert.IsFalse( player1.HasItem( item ) );

            player1.AddItem( item );
            Assert.IsTrue( player1.HasItem( item ) );

            player1.AddItem( item );
            player1.RemoveItem( item );
            Assert.IsTrue( player1.HasItem( item ) );

            player1.RemoveItem( item );
            Assert.IsFalse( player1.HasItem( item ) );
        }

        // tests our conversion to string
        [Test]
        public void TestToString()
        {
            Player player1 = Player.Create( "Joe" );
            int id = player1.id;
            Assert.AreEqual( $"{player1}", $"Joe ({id})" );
        }
    }
}
