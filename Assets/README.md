﻿# Homework

Create a new file, *Player.cs* in the *Assets/Scripts* folder so that it passes all the unit tests in *Test/TestPlayer.cs*.

To run the tests, open *Window > General > Test Runner*, then choose *Run all*.

You will be marked based on:

- passing all of the tests
- implementation of Player.cs
- code readability

# Player.cs

*Player.cs* is a class that contains:

- A static method, `Create( string )` that takes a `string` parameter (name). This method will create a new `Player`, set their `name` property with the provided `string`, and set an *incrementing* `id` property, starting from `0` (i.e. the first `Player` created has an `id` of `0`, the second has an `id` of `1`, etc)
- `name` is a readonly property - it can't be changed
- `id` is a readonly property - it can't be changed
- Creating a new `Player` via the constructor will result in `null` for the name, and `-1` for the `id`.
- The `Player` has a list of friends, that are also `Players`
- `Player` has a `numFriends` getter to return the number of friends that they have
- `Player` has an `AddFriend( Player )` method that will take a `Player` as a parameter, and add them to the friends list if they're not already there
- `Player` has a `GetFriend( int )` method that takes an `int` parameter (`id`) and returns the friend with that `id`, if we have them. If not, it will return `null`
- The `Player` has a *inventory* where we can store items using a `string` key
- `Player` has a `AddItem( string )` method that takes a `string` parameter (the item to add) and adds it to our inventory. `AddItem( string )` can be called multiple times to add more of the same item
- `Player` has a `HasItem( string )` method that takes a `string` parameter and returns `true` or `false` depending on if we have the item or not
- `Player` has a `GetNumItems( string )` method that takes a `string` parameter and returns the number of that item that we hold
- `Player` has a `RemoveItem( string )` method that takes a `string` parameter and removes an item if we have it. `RemoveItem( string )` can be called multiple times to remove more of the same item